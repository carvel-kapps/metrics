# metrics-server
Serveur de métriques des ressources Kubernetes.

```sh
ytt -f app/ --data-value namespace=kube-system | kapp deploy --app metrics --namespace=kube-system --yes
```
